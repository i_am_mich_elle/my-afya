package com.example.prac;
//package com.androiddev.loadwebpage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class aboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String url = "https://afyapap.com";

        WebView web= (WebView) findViewById(R.id.about);
        web.loadUrl(url);
    }
}
