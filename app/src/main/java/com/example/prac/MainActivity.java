
package com.example.prac;

import android.net.Uri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.view.View;
import java.net.URI;
import android.os.Handler;
public class MainActivity extends AppCompatActivity {

    private Button btnprofile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnprofile=findViewById(R.id.btnprofile);

        btnprofile.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                moveToProfile();
            }

            private void moveToProfile() {
                Intent intent = new Intent(MainActivity.this, Profile.class);

                startActivity(intent);
            }
        });
    }

    public void loadWebPage(View v){
        Intent intent =  new Intent(this, aboutActivity.class);
        startActivity(intent);
    }
}
